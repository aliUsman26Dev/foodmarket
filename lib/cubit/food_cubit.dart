import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:foodmarket/models/models.dart';
import 'package:foodmarket/services/services.dart';

part 'food_state.dart';

class FoodCubit extends Cubit<FoodState> {
  FoodCubit() : super(FoodInitial());

  Future<void> getFoods() async {
    ApiReturnValue<List<Food>> result = await FoodServices.getFood();
    if (result.value != null) {
      emit(FoodLoaded(result.value));
      print('masuk');
    } else {
      emit(FoodLoadingFiled(result.message));
      print('gagal');
    }
  }
}
