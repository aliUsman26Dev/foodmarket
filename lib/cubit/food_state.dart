part of 'food_cubit.dart';

abstract class FoodState extends Equatable {
  const FoodState();

  @override
  List<Object> get props => [];
}

class FoodInitial extends FoodState {}

class FoodLoaded extends FoodState {
  final List<Food> foods;

  FoodLoaded(this.foods);
  // print("masuk");

  @override
  List<Object> get props => [foods];
}

class FoodLoadingFiled extends FoodState {
  final String message;

  FoodLoadingFiled(this.message);

  @override
  List<Object> get props => [message];
}
