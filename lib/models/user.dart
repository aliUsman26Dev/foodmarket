part of 'models.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String email;
  final String address;
  final String houseNumber;
  final String phoneNumber;
  final String city;
  final String picturePath;

  User(
      {this.id,
      this.name,
      this.address,
      this.houseNumber,
      this.phoneNumber,
      this.city,
      this.email,
      this.picturePath});

  @override
  List<Object> get props =>
      [id, name, email, address, houseNumber, phoneNumber, city, picturePath];
}

User mockUser = User(
    id: 1,
    name: 'ali',
    address: 'Jl.Hr.Subrantas Perumahan Villa Pesona Panam dan Trilogi 2',
    houseNumber: 'F1',
    phoneNumber: '085321069056',
    city: 'Pekanbaru',
    email: 'mhdaliusmanhsb@gmail.com',
    picturePath:
        'https://i.pinimg.com/474x/8a/f4/7e/8af47e18b14b741f6be2ae499d23fcbe.jpg');
