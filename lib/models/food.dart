part of 'models.dart';

enum FoodTypes { new_food, popular, recommended }

class Food extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final String ingredients;
  final int price;
  final double rate;
  final List<FoodTypes> types;
  Food({
    this.id,
    this.picturePath,
    this.name,
    this.description,
    this.ingredients,
    this.price,
    this.rate,
    this.types = const [],
  });

  @override
  List<Object> get props =>
      [id, picturePath, name, description, ingredients, price, rate];
}

List<Food> mockFood = [
  Food(
    id: 1,
    picturePath:
        "https://www.masakapahariini.com/wp-content/uploads/2019/03/sate-sapi-bumbu-ketumbar-620x440.jpg",
    name: "Sate Sultan",
    description:
        "Sate ini tidak sembarang sate. tidak semua orang bisa makan sate ini karena sate ini tidak ada wujudnya, jangan lupa tertawa ya gays. Hidup ini hanya titipan silahkan perbanyak amal dan jangan sampai terjerumus kedalam jalan yang tidak benar, karena dimana-mana yang benar itu adalah perempuan.",
    ingredients: "daging, ayam mati, bawang, lontong, dll",
    price: 2000000,
    rate: 4.0,
    types: [FoodTypes.new_food, FoodTypes.popular, FoodTypes.recommended],
  ),
  Food(
    id: 2,
    picturePath:
        "https://cdn.popbela.com/content-images/post/20200525/00-sate-khas-indonesia-29521ed97c01d8b341fd858591e15046_750x500.jpg",
    name: "Sate Bidadari",
    description:
        "Sate ini tidak sembarang sate. tidak semua orang bisa makan sate ini karena sate ini tidak ada wujudnya, jangan lupa tertawa ya gays. Hidup ini hanya titipan silahkan perbanyak amal dan jangan sampai terjerumus kedalam jalan yang tidak benar, karena dimana-mana yang benar itu adalah perempuan.",
    ingredients: "daging, ayam mati, bawang, lontong, dll",
    price: 4000000,
    rate: 4.0,
  ),
  Food(
    id: 3,
    picturePath:
        "https://cdn.popbela.com/content-images/post/20200525/01-sate-khas-indonesia-6332ef7ef85d0762a7aeef96283d62f7.jpg",
    name: "Sate Padang",
    description:
        "Sate ini tidak sembarang sate. tidak semua orang bisa makan sate ini karena sate ini tidak ada wujudnya, jangan lupa tertawa ya gays. Hidup ini hanya titipan silahkan perbanyak amal dan jangan sampai terjerumus kedalam jalan yang tidak benar, karena dimana-mana yang benar itu adalah perempuan.",
    ingredients: "daging, ayam mati, bawang, lontong, dll",
    price: 20000,
    rate: 4.5,
    types: [FoodTypes.new_food],
  ),
  Food(
    id: 4,
    picturePath:
        "https://cdn.popbela.com/content-images/post/20200525/04-sate-khas-indonesia-cdedad3ec04595881ff8acdec26378f3.jpg",
    name: "Sate Madura",
    description:
        "Sate ini tidak sembarang sate. tidak semua orang bisa makan sate ini karena sate ini tidak ada wujudnya, jangan lupa tertawa ya gays. Hidup ini hanya titipan silahkan perbanyak amal dan jangan sampai terjerumus kedalam jalan yang tidak benar, karena dimana-mana yang benar itu adalah perempuan.",
    ingredients: "daging, ayam mati, bawang, lontong, dll",
    price: 29000000,
    rate: 5.0,
    types: [FoodTypes.recommended],
  ),
];
