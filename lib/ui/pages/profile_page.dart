part of 'pages.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Center(
            child: Image.network((context.bloc<UserCubit>().state as UserLoaded)
                .user
                .picturePath),
          ),
          Center(
            child:
                Text((context.bloc<UserCubit>().state as UserLoaded).user.name),
          ),
          Center(
            child: Text(
                (context.bloc<UserCubit>().state as UserLoaded).user.email),
          ),
        ],
      ),
    );
  }
}
