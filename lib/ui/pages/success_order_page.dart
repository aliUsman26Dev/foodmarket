part of 'pages.dart';

class SuccessOrderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IlustrationPage(
        title: "You've Mode Order",
        subtitle: "Just stay at home while we are\npreparing yourbest foods",
        picturePath: "assets/bike.png",
        buttonTitle1: "Order Other",
        buttonTap1: () {
          Get.offAll(MainPage());
        },
        buttonTap2: () {},
        buttonTitle2: "View My Order",
      ),
    );
  }
}
